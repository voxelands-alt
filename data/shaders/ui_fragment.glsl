#version 330 core

in vec2 uv;

out vec4 out_Colour;

uniform sampler2D texture0;
uniform float alphafunc;

void main(void) {

	out_Colour = texture(texture0,uv);
	if (alphafunc > 1.5) {
		float a0 = out_Colour.a;
		float a1 = texture(texture0,uv+vec2(-0.001,-0.001)).a;
		float a2 = texture(texture0,uv+vec2(-0.001,0.0)).a;
		float a3 = texture(texture0,uv+vec2(-0.001,0.001)).a;
		float a4 = texture(texture0,uv+vec2(0.0,-0.001)).a;
		float a5 = texture(texture0,uv+vec2(0.0,0.001)).a;
		float a6 = texture(texture0,uv+vec2(0.001,-0.001)).a;
		float a7 = texture(texture0,uv+vec2(0.001,0.0)).a;
		float a8 = texture(texture0,uv+vec2(0.001,0.001)).a;
		out_Colour.a = smoothstep(0.45,0.55,(a0+a1+a2+a3+a4+a5+a6+a7+a8)/9.0);
	}else if (alphafunc > 0.5) {
		out_Colour.a = smoothstep(0.45,0.55,out_Colour.a);
	}
}
