#version 330 core

in vec2 uv;
in vec3 normal;
in vec3 to_cam;
in vec3 light0_to;

out vec4 final_colour;

uniform sampler2D texture0;

vec4 light0_colour = vec4(1.0,1.0,1.0,1.0);
float light0_att = 0.0;

void main(void)
{
	vec3 n = normalize(normal);
	vec3 diffuse = vec3(0.0);

	float att = 1.0;
	if (light0_att > 0.0) {
		float dist = length(light0_to);
		if (dist > light0_att) {
			att = 0.0;
		}else{
			att = 1.0-((1.0/light0_att)*dist);
		}
	}
	if (att > 0.0) {
		vec3 to = normalize(light0_to);
		diffuse += max((max(dot(n,to),0.0)*light0_colour.rgb)*att,0.2);
	}else{
		diffuse += vec3(0.2);
	}

	vec4 texdata = texture(texture0,uv);

	final_colour = vec4(diffuse,1.0)*texdata;
}
