#version 330 core

in vec3 position;
in vec3 normals;
in vec2 texcoords;

out vec2 uv;
out vec3 normal;
out vec3 to_cam;
out vec3 light0_to;

uniform mat4 transformationMatrix;
uniform mat4 projectionMatrix;
uniform mat4 viewMatrix;

uniform vec4 plane;

vec3 light0_pos = vec3(-5.0,500.0,500.0);

void main(void)
{
	vec4 worldpos = transformationMatrix*vec4(position, 1.0);

	gl_ClipDistance[0] = dot(worldpos,plane);

	gl_Position = projectionMatrix*viewMatrix*worldpos;

	uv = texcoords;
	normal = (transformationMatrix*vec4(normals,0.0)).xyz;

	light0_to = light0_pos - worldpos.xyz;

	to_cam = (inverse(viewMatrix)*vec4(0.0,0.0,0.0,1.0)).xyz - worldpos.xyz;
}
