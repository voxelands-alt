/************************************************************************
* main.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "path.h"
#include "net.h"
#include "content.h"
#define _WM_EXPOSE_ALL
#include "wm.h"
#include "graphics.h"
#include "ui.h"
#include "client.h"
#include "map.h"

static int state_client = VLSTATE_MENU;

/* get/set the client state */
int client_state(int s)
{
	int r = state_client;
	if (s != VLSTATE_GET)
		state_client = s;
	return r;
}

int main(int argc, char** argv)
{
	material_t *steel;
	material_t *glass;
	material_t *ice;
	material_t *mud;
	textbuffer_t tb;
	font_t *f;
	char buff[256];
	colour_t glasscolour;
	colour_t icecolour;
	camera_t *c;

	command_init();
	path_init();
	config_init(argc,argv);
	events_init();
	time_init();
	intl_init();
	sys_console_init();
	net_init();
	content_init();
	client_init();

	wm_init();
	wm_title(PACKAGE " - " VERSION);
	ui_init();

	ui_loader("Loading...");



	f = font_find("default");

	textbuffer_init(&tb,f,15,20,20,0,0,0,0);

	glasscolour.r = 255;
	glasscolour.g = 255;
	glasscolour.b = 255;
	glasscolour.a = 10;

	icecolour.r = 214;
	icecolour.g = 220;
	icecolour.b = 255;
	icecolour.a = 220;

	steel = mat_from_image("texture","steel_block.png");
	mud = mat_from_image("texture","mud.png");
	glass = mat_from_colour(&glasscolour);
	ice = mat_from_colour(&icecolour);

	mat_shininess(steel,5.0,0.5);
	mat_bumpiness(steel,0.1);

	mat_bumpiness(mud,0.8);

	mat_options(ice,MATOPT_ALPHA_BLEND);
	mat_shininess(ice,8.0,0.2);

	mat_options(glass,MATOPT_ADDITIVE_BLEND);
	mat_shininess(glass,2.0,0.8);

	c = camera_get();
	c->x = 0.0;
	c->y = 20.6;
	c->z = 0.0;
	c->pitch = 20.0;



	client_connect_singleplayer("test world");

	while (state_client != VLSTATE_EXIT) {
		render_pre();

		if (state_client == VLSTATE_PLAY) {
			textbuffer_clear(&tb);
			c = camera_get();
			sprintf(buff,"FPS: %d (%.1f, %.1f, %.1f) %.1f %.1f",wm_data.fps,c->x,c->y,c->z,c->yaw,c->pitch);
			textbuffer_addstr(&tb,buff);
			render2d_textbuffer(&tb);
		}

		render_post();
	}

	ui_loader("Shutting Down...");

	client_disconnect();

	content_exit();
	net_exit();
	config_save(NULL,NULL,NULL);
	events_exit();
	path_exit();

	return 0;
}

#ifdef WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	LPWSTR *args;
	char** argv;
	int argc = 0;

	args = CommandLineToArgvW(GetCommandLineW(), &argc);
	if (args) {
		argv = (char**)args;
	}else{
		argv = malloc(sizeof(char*));
		argv[0] = NULL;
	}
	return main(argc,argv);
}
#endif
