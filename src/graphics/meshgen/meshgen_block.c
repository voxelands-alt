/************************************************************************
* meshgen_block.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "graphics.h"

#include "meshgen.h"
#include "map.h"

/*
 * LOD
 * 0 - detail overlay	0-24 blocks
 * 1 - standard LOD	0-48 blocks
 * 2 - low detail	48-96 blocks
 * 3 - distance		96+ blocks
 */

/* generate the mesh for a block */
int meshgen_block(mapobj_t *o, block_t *b, v3_t *p, pos_t *bp)
{
	contentfeatures_t *f;

	f = content_features(b->content);

	if (!f)
		return 0;

	switch (f->draw_type) {
	case CDT_AIRLIKE:
		return 0;
		break;
	case CDT_CUBELIKE:
		meshgen_cubelike(o,b,p,bp,1);
		break;
	case CDT_RAILLIKE:
		break;
	case CDT_PLANTLIKE:
		break;
	case CDT_PLANTLIKE_FERN:
		break;
	case CDT_MELONLIKE:
		break;
	case CDT_LIQUID:
		break;
	case CDT_LIQUID_SOURCE:
		break;
	case CDT_NODEBOX:
		break;
	case CDT_GLASSLIKE:
		break;
	case CDT_TORCHLIKE:
		break;
	case CDT_FENCELIKE:
		break;
	case CDT_FIRELIKE:
		break;
	case CDT_WALLLIKE:
		break;
	case CDT_ROOFLIKE:
		break;
	case CDT_LEAFLIKE:
		break;
	case CDT_NODEBOX_META:
		break;
	case CDT_WIRELIKE:
		break;
	case CDT_3DWIRELIKE:
		break;
	case CDT_STAIRLIKE:
		break;
	case CDT_SLABLIKE:
		break;
	case CDT_TRUNKLIKE:
		break;
	case CDT_DIRTLIKE:
		/* TODO: make dirtlike */
		meshgen_cubelike(o,b,p,bp,1);
		break;
	case CDT_FLAGLIKE:
		break;
	}

	return 1;
}

/* this creates a "default mesh" of sorts, used for weilded items etc */
void meshgen_single(object_t *o, block_t *b)
{
	/* TODO: this */
}
