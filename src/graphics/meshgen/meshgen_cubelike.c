/************************************************************************
* meshgen_cubelike.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "graphics.h"

#include "meshgen.h"
#include "map.h"

static void meshgen_cubelike_lod1(mapobj_t *o, block_t *b, v3_t *p, pos_t *bp, uint8_t lod)
{
	uint8_t f[6];
	v3_t v;
	v3_t n;
	v2_t t;
	int i;

	meshgen_hardfaces(o->chunk,b,bp,f);

	if (f[MAP_X_MINUS_INDEX]) {
		mesh_t *m = meshgen_get_mesh(o,b,MAP_X_MINUS,lod);
		if (m) {
			i = m->v->length/3;

			v.x = p->x-0.5;
			v.y = p->y+0.5;
			v.z = p->z+0.5;

			n.x = -1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y+0.5;
			v.z = p->z-0.5;

			n.x = -1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y-0.5;
			v.z = p->z-0.5;

			n.x = -1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y-0.5;
			v.z = p->z+0.5;

			n.x = -1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			array_push_int(m->i,i+0);
			array_push_int(m->i,i+1);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+3);
			array_push_int(m->i,i+0);
		}
	}
	if (f[MAP_X_PLUS_INDEX]) {
		mesh_t *m = meshgen_get_mesh(o,b,MAP_X_PLUS,lod);
		if (m) {
			i = m->v->length/3;

			v.x = p->x+0.5;
			v.y = p->y-0.5;
			v.z = p->z+0.5;

			n.x = 1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y-0.5;
			v.z = p->z-0.5;

			n.x = 1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y+0.5;
			v.z = p->z-0.5;

			n.x = 1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y+0.5;
			v.z = p->z+0.5;

			n.x = 1.0;
			n.y = 0.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			array_push_int(m->i,i+0);
			array_push_int(m->i,i+1);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+3);
			array_push_int(m->i,i+0);
		}
	}
	if (f[MAP_Y_MINUS_INDEX]) {
		mesh_t *m = meshgen_get_mesh(o,b,MAP_Y_MINUS,lod);
		if (m) {
			i = m->v->length/3;

			v.x = p->x+0.5;
			v.y = p->y-0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = -1.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y-0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = -1.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y-0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = -1.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y-0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = -1.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			array_push_int(m->i,i+0);
			array_push_int(m->i,i+1);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+3);
			array_push_int(m->i,i+0);
		}
	}
	if (f[MAP_Y_PLUS_INDEX]) {
		mesh_t *m = meshgen_get_mesh(o,b,MAP_Y_PLUS,lod);
		if (m) {
			i = m->v->length/3;

			v.x = p->x+0.5;
			v.y = p->y+0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = 1.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y+0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = 1.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y+0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = 1.0;
			n.z = 0.0;

			t.x = 0.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y+0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = 1.0;
			n.z = 0.0;

			t.x = 1.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			array_push_int(m->i,i+0);
			array_push_int(m->i,i+1);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+3);
			array_push_int(m->i,i+0);
		}
	}
	if (f[MAP_Z_MINUS_INDEX]) {
		mesh_t *m = meshgen_get_mesh(o,b,MAP_Z_MINUS,lod);
		if (m) {
			i = m->v->length/3;

			v.x = p->x-0.5;
			v.y = p->y+0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = -1.0;

			t.x = 0.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y+0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = -1.0;

			t.x = 1.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y-0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = -1.0;

			t.x = 1.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y-0.5;
			v.z = p->z-0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = -1.0;

			t.x = 0.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			array_push_int(m->i,i+0);
			array_push_int(m->i,i+1);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+3);
			array_push_int(m->i,i+0);
		}
	}
	if (f[MAP_Z_PLUS_INDEX]) {
		mesh_t *m = meshgen_get_mesh(o,b,MAP_Z_PLUS,lod);
		if (m) {
			i = m->v->length/3;

			v.x = p->x+0.5;
			v.y = p->y+0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = 1.0;

			t.x = 0.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y+0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = 1.0;

			t.x = 1.0;
			t.y = 0.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x-0.5;
			v.y = p->y-0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = 1.0;

			t.x = 1.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			v.x = p->x+0.5;
			v.y = p->y-0.5;
			v.z = p->z+0.5;

			n.x = 0.0;
			n.y = 0.0;
			n.z = 1.0;

			t.x = 0.0;
			t.y = 1.0;

			array_push_v3t(m->v,&v);
			array_push_v3t(m->n,&n);
			array_push_v2t(m->t,&t);

			array_push_int(m->i,i+0);
			array_push_int(m->i,i+1);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+2);
			array_push_int(m->i,i+3);
			array_push_int(m->i,i+0);
		}
	}
}

void meshgen_cubelike(mapobj_t *o, block_t *b, v3_t *p, pos_t *bp, uint8_t lod)
{
	if (!lod) {
		meshgen_cubelike_lod1(o,b,p,bp,0);
		return;
	}

	meshgen_cubelike_lod1(o,b,p,bp,1);
	meshgen_cubelike_lod1(o,b,p,bp,2);
}
