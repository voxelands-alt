/************************************************************************
* camera.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _WM_EXPOSE_ALL
#include "wm.h"

static camera_t camera = {
	0.0,
	0.0,
	0.0,
	0.0,
	0.0
};

/* move to the camera position */
void camera_view_matrix(matrix_t *mat, camera_t *cam)
{
	if (!cam)
		cam = &camera;
	matrix_init(mat);
	matrix_translate(mat,-cam->x, -cam->y, -cam->z);
	matrix_rotate_deg_y(mat,-cam->yaw);
	matrix_rotate_deg_x(mat,cam->pitch);
}

/* get the camera data */
camera_t *camera_get()
{
	return &camera;
}

/* set the position of the camera */
void camera_set_pos(v3_t *p)
{
	camera.x = p->x;
	camera.y = p->y;
	camera.z = p->z;
}

/* set the yaw - y rotation - of the camera */
void camera_set_yaw(GLfloat yaw)
{
	camera.yaw = yaw;
}

/* set the pitch - x rotation - of the camera */
void camera_set_pitch(GLfloat pitch)
{
	camera.pitch = pitch;
}
