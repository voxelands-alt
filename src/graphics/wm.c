/************************************************************************
* wm.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _WM_EXPOSE_ALL
#include "wm.h"
#include "graphics.h"

#include <string.h>

wm_t wm_data = {
	{
		600,
		1024,
		0,
		0
	},
	0,
	30,
	{30,30,30,30},
	0,
	30,
	NULL,
	1000.0,
	0
};

/* command width setter */
int wm_width_setter(char* value)
{
	int v = strtol(value,NULL,10);
	if (v > 0) {
		wm_data.size.width = v;
		wm_resize();
	}
	return 0;
}

/* command height setter */
int wm_height_setter(char* value)
{
	int v = strtol(value,NULL,10);
	if (v > 0) {
		wm_data.size.height = v;
		wm_resize();
	}
	return 0;
}

/* command frame cap setter */
int wm_cap_setter(char* value)
{
	int v = strtol(value,NULL,10);
	if (v > 0)
		wm_data.frame_cap = v;
	return 0;
}

/* command fullscreen setter */
int wm_fullscreen_setter(char* value)
{
	int v = !!strtol(value,NULL,10);
	wm_toggle_fullscreen(v);
	return 0;
}

/* screen capture - take a screenshot, save to file */
void wm_capture(char* file)
{
	char* fmt;
	image_t *p;

	p = image_load_fromscreen(0,0,wm_data.size.width,wm_data.size.height,0);
	if (!p) {
		vlprintf(CN_ERROR, "Could not grab screen data");
		return;
	}

	fmt = config_get("wm.capture_format");
	if (!fmt || !strcmp(fmt,"png")) {
		if (!file)
			file = "screenshot.png";
		image_save_png(p,file);
	}else if (!strcmp(fmt,"tga")) {
		if (!file)
			file = "screenshot.tga";
		image_save_tga(p,file);
	}else if (!strcmp(fmt,"bmp")) {
		if (!file)
			file = "screenshot.bmp";
		image_save_bmp(p,file);
	}else{
		vlprintf(CN_WARN, "Unsupported capture format '%s'",fmt);
	}

	free(p->pixels);
	free(p);
}
