/************************************************************************
* mapgen_util.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "array.h"

#define _MAPGEN_LOCAL
#include "map.h"

/* fill a chunk with a given block type */
void mapgen_fill_chunk(chunk_t *ch, content_t id)
{
	int x;
	int y;
	int z;
	block_t *b;

	for (x=0; x<CHUNKSIZE; x++) {
		for (y=0; y<CHUNKSIZE; y++) {
			for (z=0; z<CHUNKSIZE; z++) {
				b = &ch->blocks[x][y][z];
				b->content = id;
				b->param1 = 0;
				b->param2 = 0;
				b->param3 = 0;
			}
		}
	}
}

/* create an empty chunk */
chunk_t *mapgen_create_chunk(pos_t *p)
{
	chunk_t *ch;

	ch = malloc(sizeof(chunk_t));
	if (!ch)
		return NULL;

	mapgen_fill_chunk(ch,CONTENT_IGNORE);

	ch->pos.x = p->x;
	ch->pos.y = p->y;
	ch->pos.z = p->z;

	ch->timestamp = 0;
	ch->idletime = 0.0;
	ch->state = CHUNK_STATE_WRITE_AT_UNLOAD|CHUNK_STATE_UNGENERATED|CHUNK_STATE_BADSPAWN|CHUNK_STATE_BADLIGHT|CHUNK_STATE_BADMESH;

	ch->mesh.obj = NULL;
	ch->mesh.mut = NULL;
	array_init(&ch->mesh.sounds,ARRAY_TYPE_PTR);

	array_init(&ch->mobs.data,ARRAY_TYPE_PTR);

	array_init(&ch->metadata,ARRAY_TYPE_PTR);

	return ch;
}
