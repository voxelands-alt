/************************************************************************
* content_craftitem.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _CONTENT_LOCAL
#include "content.h"

static void content_craftitem_defaults(contentfeatures_t *f)
{
	content_defaults(f);
	/* TODO: set texture unknown_item.png */
	f->data.craftitem.stackable = 1;
	f->data.craftitem.consumable = 0;
	f->data.craftitem.hunger_effect = 0;
	f->data.craftitem.health_effect = 0;
	f->data.craftitem.cold_effect = 0;
	f->data.craftitem.energy_effect = 0;
	f->data.craftitem.drop_count = -1;
	f->data.craftitem.teleports = -2;
	f->data.craftitem.drop_item = CONTENT_IGNORE;
	f->data.craftitem.thrown_item = CONTENT_IGNORE;
	f->data.craftitem.shot_item = CONTENT_IGNORE;
}

int content_craftitem_init()
{
	int i;
	contentfeatures_t *f;
	for (i=0; i<CONTENT_ARRAY_SIZE; i++) {
		f = &contentfeatures[CONTENT_INDEX_CRAFTITEM][i];
		content_craftitem_defaults(f);
	}

	return 0;
}
