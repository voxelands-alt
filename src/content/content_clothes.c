/************************************************************************
* content_clothes.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#define _CONTENT_LOCAL
#include "content.h"

static void content_clothes_defaults(contentfeatures_t *f)
{
	content_defaults(f);
	/* TODO: set texture unknown_item.png (unknown_clothes.png?) */
	/* the type of this clothing */
	f->data.clothes.type = CCT_NONE;
	/* the strength as armour */
	f->data.clothes.armour = 0.0;
	/* the effectiveness against the cold zone */
	f->data.clothes.warmth = 0.0;
	/* the effectiveness against vacuum / space */
	f->data.clothes.vacuum = 0.0;
	/* the effectiveness against suffocation */
	f->data.clothes.suffocate = 0.0;
	/* this determines how fast the item wears out from use */
	f->data.clothes.durability = 5;
	/* for medallions, how much the affect durability of other items */
	f->data.clothes.effect = 1.0;
}

int content_clothes_init()
{
	int i;
	contentfeatures_t *f;
	for (i=0; i<CONTENT_ARRAY_SIZE; i++) {
		f = &contentfeatures[CONTENT_INDEX_CLOTHES][i];
		content_clothes_defaults(f);
	}

	return 0;
}
