/************************************************************************
* compat.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "content.h"

/* these are dummy functions, so config_default.c and command.c don't
 * have to be compiled twice, and all commands work even if they do
 * nothing */

/* setters */
int wm_width_setter(char* value)
{
	return 0;
}
int wm_height_setter(char* value)
{
	return 0;
}
int wm_cap_setter(char* value)
{
	return 0;
}
int wm_fullscreen_setter(char* value)
{
	return 0;
}
int opengl_anisotropic_setter(char* value)
{
	return 0;
}
int opengl_bilinear_setter(char* value)
{
	return 0;
}
int opengl_trilinear_setter(char* value)
{
	return 0;
}
int opengl_mipmap_setter(char* value)
{
	return 0;
}
int opengl_particles_setter(char* value)
{
	return 0;
}
int opengl_particles_max_setter(char* value)
{
	return 0;
}
int opengl_bumpmap_setter(char* value)
{
	return 0;
}
int opengl_shadowpass_setter(char* value)
{
	return 0;
}
int opengl_shadowsize_setter(char* value)
{
	return 0;
}
int opengl_psdf_setter(char* value)
{
	return 0;
}
int ui_scale_setter(char* value)
{
	return 0;
}
int ui_autoscale_setter(char* value)
{
	return 0;
}

/* commands */
int event_bind(array_t *args)
{
	return 0;
}

/* other */
void events_save(file_t *f)
{
}

/* meshgen */
void content_meshgen_textures(contentfeatures_t *f,...)
{
}
