/************************************************************************
* servermain.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"
#include "path.h"
#include "net.h"
#include "content.h"

int main(int argc, char** argv)
{

	command_init();
	path_init();
	config_init(argc,argv);
	time_init();
	intl_init();
	sys_console_init();
	net_init();
	content_init();

	/* init server */

	/* start game server */

	/* start http server */

	/* main loop */

	while (1) {
	}

	/* stop http server */

	/* stop game server */

	/* exit server */

	content_exit();
	net_exit();
	config_save(NULL,NULL,NULL);
	path_exit();

	return 0;
}
