/************************************************************************
* sys_console.c
* voxelands - 3d voxel world sandbox game
* Copyright (C) Lisa 'darkrose' Milne 2016 <lisa@ltmnet.com>
*
* This program is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful, but
* WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
* See the GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program.  If not, see <http://www.gnu.org/licenses/>
************************************************************************/

#include "common.h"

#ifdef WIN32

void sys_console_print(char* str, int newline)
{
}

void sys_console_printf(char* fmt, ...)
{
}

void sys_console_init()
{
}

void sys_console_exit()
{
}

#else

/* at present this is just an over-complicated printf
 * it should become an interective shell for dealing with the server */

#include <stdio.h>
#include <stdarg.h>
#include <string.h>
#include <unistd.h>
#include <sys/ioctl.h>

/* add a single line of text to the system console */
static void sys_console_addline(char* str)
{
	printf("%s\n",str);
}

/* print text to the system console */
void sys_console_print(char* str, int newline)
{
	char buff[2048];
	int l;
	int i;
	int p;
	char* b;
	char* e;
	int max;
	int cols = 80;
#ifdef TIOCGSIZE
	struct ttysize ts;
	ioctl(STDIN_FILENO, TIOCGSIZE, &ts);
	cols = ts.ts_cols;
#else
#ifdef TIOCGWINSZ
	struct winsize ts;
	ioctl(STDIN_FILENO, TIOCGWINSZ, &ts);
	cols = ts.ws_col;
#endif
#endif
	max = cols*4;

	if (max > 2047) {
		max = 2047;
		cols = 511;
	}

	b = str;
	while (b && b[0]) {
		e = utf8_strchr(b,'\n',NULL);
		if (e) {
			l = e-b;
			p = 1;
			if (l > max) {
				e = b+max;
				l = max;
				p = 0;
			}
			strncpy(buff,b,l);
			buff[l] = 0;
			i = l;
			l = utf8_strlen(buff);
			while (l > cols) {
				utf8_dec(buff,&i);
				l = utf8_offset(buff,i);
				buff[i] = 0;
			}
			if (p)
				utf8_inc(b,&i);
			b += i;
		}else{
			if (strlen(b) > max) {
				strncpy(buff,b,max);
				buff[max] = 0;
				i = l;
				l = utf8_strlen(buff);
				while (l > cols) {
					utf8_dec(buff,&i);
					l = utf8_offset(buff,i);
					buff[i] = 0;
				}
				b += strlen(buff);
			}else{
				strcpy(buff,b);
				b = NULL;
			}
		}
		sys_console_addline(buff);
	}
}

/* print formatted text to the system console */
void sys_console_printf(char* fmt, ...)
{
	char buff[1024];
	va_list ap;

	if (!fmt)
		return;

	va_start(ap, fmt);

	if (vsnprintf(buff, 1024, fmt, ap) >= 1024) {
		va_end(ap);
		return;
	}

	va_end(ap);

	sys_console_print(buff, 0);
}

void sys_console_init()
{
	sys_console_print("\n\n",0);
	sys_console_print("___    ___                 __\n",0);
	sys_console_print("\\  \\  /  /______  __ ____ |  | ___   ____  ___  ______\n",0);
	sys_console_print(" \\  \\/  /  _  \\ \\/ // __ \\|  |/ _ \\ /    \\|   \\/  ___/\n",0);
	sys_console_print("  \\    /| |_| |    \\  ___/|  |  _  \\   |  \\ |  |___ \\\n",0);
	sys_console_print("   \\  / \\___  >_/\\  > __  >__|_/ \\  >__|  /___/____  >\n",0);
	sys_console_print("    \\/      \\/    \\/    \\/        \\/    \\/         \\/\n",0);
	sys_console_printf("\nVersion: %s\n",VERSION);
}

void sys_console_exit()
{
}

#endif
