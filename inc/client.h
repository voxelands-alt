#ifndef _CLIENT_H_
#define _CLIENT_H_

#include "map.h"

/* defined in client.c */
int client_init(void);
int client_connect_singleplayer(char* world);
int client_connect_multiplayer(char* address, char* port);
void client_disconnect(void);

#endif
