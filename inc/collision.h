#ifndef _COLLISION_H_
#define _COLLISION_H_

#include "common.h"

enum {
	COLLISION_FALL,
	COLLISION_NODE
};

#ifndef _HAVE_COLLISIONINFO_TYPE
#define _HAVE_COLLISIONINFO_TYPE
typedef struct collisioninfo_s {
	struct collisioninfo_s *prev;
	struct collisioninfo_s *next;
	int type;
	float speed;
	pos_t pos;
	v3_t speed_o;
	v3_t speed_n;
} collisioninfo_t;
#endif

#ifndef _HAVE_COLLISIONRESULT_TYPE
#define _HAVE_COLLISIONRESULT_TYPE
typedef struct collisionresult_s {
	uint8_t touching_ground;
	uint8_t in_liquid;
	uint8_t touching_lethal;
	uint8_t collides;
	uint8_t collides_xz;
	uint8_t standing_on_unloaded;
	collisioninfo_t *collisions;
} collisionresult_t;
#endif

#ifndef _HAVE_COLLISIONBLOCK_TYPE
#define _HAVE_COLLISIONBLOCK_TYPE
typedef struct collisionblock_s {
	pos_t pos;
	aabox_t box;
	uint8_t is_unloaded;
	uint8_t is_stepup;
} collisionblock_t;
#endif

collisionresult_t *collision_move(float pos_max_d, aabox_t *box, float stepheight, float dtime, v3_t *pos, v3_t *speed, v3_t accel);
void collision_free(collisionresult_t *r);

#endif
