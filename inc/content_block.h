#ifndef _CONTENT_BLOCK_H_
#define _CONTENT_BLOCK_H_

#include "content.h"

/* block ids 0x000 - 0xFFF */

#define CONTENT_AIR	0x000
#define CONTENT_STONE	0x001
#define CONTENT_MUD	0x002
#define CONTENT_GRASS	0x003

/* defined in block_ground.c */
int block_ground_init(void);

#endif

